<?php
session_start();
require "koneksi.php";
$format = $_GET['format'];
$menu = $_GET['menu'];
if($format == "pdf"){
  require "Vendor/mc_table.php";
  $pdf=new PDF_MC_Table();
  $pdf->AddPage('L');
  $pdf->SetFont('Helvetica', '', 8);
  if($menu == "sanksi"){
    $querysanksi = mysqli_query($koneksi, $_SESSION['Query']);
    $pdf->SetWidths(array(15, 15, 15, 15, 15, 20, 18, 15, 20, 15, 15, 15, 15, 15, 20, 20, 20));
    $pdf->Row(array('Jenis', 'Sandi Pelapor', 'Nama Pelapor', 'Bulan Data' , 'Kategori Sebab', 'Kategori Sanksi', 'Sanksi', 'Memo Rekom', 'No. Surat Sanksi', 'Tgl. Surat', 'Status Pantau', 'Realisasi', 'Tgl. Bayar', 'Rekening Antara', 'Rekenening Penerimaan', 'Keterangan', 'File'));
    while($isi = mysqli_fetch_array($querysanksi)){
	  	$pdf->Row(array($isi['jenispelaporan'], $isi['sandipelapor'], $isi['namapelapor'], $isi['bulandata'], $isi['kategorisebab'], $isi['kategorisanksi'], $isi['sanksi'], $isi['memorekam'], $isi['suratsanksi'], $isi['tglsurat'], $isi['statuspantau'], $isi['realisasi'], $isi['tglbayar'], $isi['rekeningantara'], $isi['rekeningpenerimaan'], $isi['ket'], $isi['file']));
    }
  }
  else if($menu == "anggaran"){
    $queryanggaran = mysqli_query($koneksi, $_SESSION['Query']);
    $pdf->SetWidths(array(15, 15, 15, 15, 15, 20, 18, 15, 20, 15, 15, 15, 15, 15, 20, 20, 20));
    $pdf->Row(array('Ket. (Kegiatan / Pelaku PDDN)', 'Tgl. WRA', 'Nominal WRA', 'Tgl. Pertanggungjawaban' , 'Nominal Pertanggungjawaban', 'Tgl. Input Bijak', 'Status Anggaran', 'Status Bijak', 'Unit Kerja'));
    while($isi = mysqli_fetch_array($queryanggaran)){
	  	$pdf->Row(array($isi['ketkegiatanpelakupddn'], $isi['tglwra'], $isi['nominalwra'], $isi['tglpertanggungjawaban'], $isi['nominalpertanggungjawaban'], $isi['tglinputbijak'], $isi['statusanggaran'], $isi['statusbijak'], $isi['unitkerja']));
    }
  }  
  $pdf->Output();
}
else if($format == "xls"){
  header("Content-Type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=KPwBI_Solo-".$menu."-".preg_replace('/\s+/', '_', $_SESSION['Name']).".xls");
  if($menu == "sanksi"){
    $querysanksi = mysqli_query($koneksi, $_SESSION['Query']);
?>
<table border="1">
	<thead>
		<tr>
			<td rowspan="2">Jenis</td>
	    <td colspan="2">Pelapor</td>
			<td rowspan="2">Bulan Data</td>
			<td colspan="2">Kategori</td>
			<td rowspan="2">Sanksi</td>
			<td rowspan="2">Memo Rekom</td>
			<td rowspan="2">No. Surat Sanksi</td>
			<td rowspan="2">Tgl. Surat</td>
			<td rowspan="2">Status Pantau</td>
			<td rowspan="2">Realisasi</td>
			<td rowspan="2">Tgl. Bayar</td>
			<td rowspan="2">Rekening Antara</td>
			<td rowspan="2">Rekening Penerimaan</td>
			<td rowspan="2">Keterangan</td>
			<td rowspan="2">File</td>
		</tr>
  	<tr>
			<td>Sandi</td>
			<td>Nama</td>
      <td>Sebab</td>
      <td>Sanksi</td>
		</tr>
	</thead>
	<tbody>
<?php
    while($isi = mysqli_fetch_array($querysanksi)){
?>
	<tr>
	  <td><?php echo $isi['jenispelaporan'] ?></td>
	  <td><?php echo $isi['sandipelapor'] ?></td>
	  <td><?php echo $isi['namapelapor'] ?></td>
	  <td><?php echo $isi['bulandata'] ?></td>
		<td><?php echo $isi['kategorisebab'] ?></td>
	  <td><?php echo $isi['kategorisanksi'] ?></td>
		<td><?php echo $isi['sanksi'] ?></td>
		<td><?php echo $isi['memorekam'] ?></td>
	  <td><?php echo $isi['suratsanksi'] ?></td>
		<td><?php echo $isi['tglsurat'] ?></td>
		<td><?php echo $isi['statuspantau'] ?></td>
		<td><?php echo $isi['realisasi'] ?></td>
		<td><?php echo $isi['tglbayar'] ?></td>
		<td><?php echo $isi['rekeningantara'] ?></td>
	  <td><?php echo $isi['rekeningpenerimaan'] ?></td>
	  <td><?php echo $isi['ket'] ?></td>
		<td><?php echo $isi['file'] ?></td>
	</tr>
<?php
    }
?>
	</tbody>
</table>
<?php
  }
  else if($menu == "anggaran"){
    $queryanggaran = mysqli_query($koneksi, $_SESSION['Query']);
?>
<table border="1">
	<thead>
		<tr>
			<td rowspan="2">Ket. (Kegiatan / Pelaku PDDN)</td>
	    <td rowspan="2">Tgl. WRA</td>
			<td rowspan="2">Nominal WRA</td>
			<td rowspan="2">Tgl. Pertanggungjawaban</td>
			<td rowspan="2">Nominal Pertanggungjawaban</td>
			<td rowspan="2">Tgl. Input Bijak</td>
			<td colspan="2">Status</td>
      <td rowspan="2">Id Unit Kerja</td>
		</tr>
  	<tr>
			<td>Anggaran</td>
			<td>Bijak</td>
		</tr>
	</thead>
<tbody>
<?php
    while($isi = mysqli_fetch_array($queryanggaran)){
?>
	<tr>
	  <td><?php echo $isi['ketkegiatanpelakupddn']; ?></td>
	  <td><?php echo $isi['tglwra']; ?></td>
	  <td><?php echo "Rp. ".$isi['nominalwra'].",-"; ?></td>
	  <td><?php echo $isi['tglpertanggungjawaban']; ?></td>
		<td><?php echo "Rp. ".$isi['nominalpertanggungjawaban'].",-"; ?></td>
	  <td><?php echo $isi['tglinputbijak']; ?></td>
		<td><?php echo $isi['statusanggaran']; ?></td>
		<td><?php echo $isi['statusbijak']; ?></td>
	  <td><?php echo $isi['unitkerja']; ?></td>
	</tr>
<?php
    }
?>
	</tbody>
</table>
<?php
  }
}
?> 