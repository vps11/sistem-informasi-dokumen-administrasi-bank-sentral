<?php
if($_SESSION['IdRole'] == 2){
  if($menu == "sanksi"){
?>
<div class="x_title">
	<h2>Tambah Administrasi Sanksi</h2>
</div>
<form action="simpan.php?menu=sanksi" method="POST" class="form-horizontal form-label-left" id="myForm" enctype="multipart/form-data">
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Jenis Pelaporan</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="jenispelaporan" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
	</div><!-- jenispelaporan -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Sandi Pelapor</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="sandipelapor" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- sandipelapor -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Pelapor</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="namapelapor" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- namapelapor -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Bulan Data</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="bulandata" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- bulandata -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori Sebab</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="katsebab" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- katsebab -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Kategori Sanksi</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="katsanksi" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- katsanksi -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Sanksi</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" onchange="splitInDots(this)" name="sank" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- sank -->
  <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Memo Rekom</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="text"  name="memorekam" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- memorekam -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Surat Sanksi</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="text"  name="srtsanksi" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- srtsanksi -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Surat</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input id="datecreate" type="text"  name="tglsurat" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- tglsurat -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status Pantau</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<select name="statuspantau" class="form-control col-md-7 col-xs-12">
				<option value="">-</option>
				<option value="Telah Ditegurkan">Telah Ditegurkan</option>
				<option value="Telah Dibayar">Telah Dibayar</option>
      </select>
  	</div>
  </div><!-- statuspantau -->
  <div class="item form-group">
	  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Realisasi Akhir</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="text"  name="realisasi" onchange="splitInDots(this)" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- realisasi -->
  <div class="item form-group">
  	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Bayar</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="text" name="tglbayar"  class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- tglbayar -->
  <script>
$('input[name="tglsurat"]').daterangepicker({
  "singleDatePicker": true,
  "showDropdowns": true,
  "autoApply": true,
  "locale":{
    "format": "MM/DD/YYYY",
    "separator": " - ",
    "daysOfWeek":["M", "S", "S", "R", "K", "J", "S"],
    "monthNames":["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    "firstDay": 1
  }
},
  function(start, end, label){
    console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
  }
);
  </script>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Rekening Antara</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="text"  name="rekantara"  class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- rekantara -->
  <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Rekening Penerimaan</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="text"  name="rekpenerima"  class="form-control col-md-7 col-xs-12" value=""></input>
  	</div>
  </div><!-- rekpenerima -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Keterangan</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="text"  name="ket"  class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- ket -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name"><i>File</i></label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
			<input type="file" name="file" value=""></input>
		</div>
  </div><!-- file -->
  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button id="send" type="submit" name="sanksi" class="btn btn-success">Simpan</button>
    </div>
  </div>
</form>
<?php
  }
  else if($menu == 'anggaran'){
?>
<div class="x_title">
	<h2>Tambah Administrasi Anggaran</h2>
</div>
<form action="simpan.php?menu=anggaran" method="POST" class="form-horizontal form-label-left" id="myForm">
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Keterangan (Kegiatan / Pelaku PDDN)</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<textarea name="ketkegiatanpelakupddn" class="form-control col-md-7 col-xs-12"></textarea>
		</div>
	</div><!-- ketkegiatanpelakupddn -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal WRA</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="tglwra" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- tglwra -->
  <script>
$('input[name="tglwra"]').daterangepicker({
  "singleDatePicker": true,
  "showDropdowns": true,
  "autoApply": true,
  "locale":{
    "format": "MM/DD/YYYY",
    "separator": " - ",
    "daysOfWeek":["M", "S", "S", "R", "K", "J", "S"],
    "monthNames":["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    "firstDay": 1
  }
},
  function(start, end, label){
    console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
  }
);
  </script>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nominal WRA</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" onchange="splitInDots(this)" name="nominalwra" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- nominalwra -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Pertanggungjawaban</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="tglpertanggungjawaban" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- tglpertanggungjawaban -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nominal Pertanggungjawaban</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text" onchange="splitInDots(this)" name="nominalpertanggungjawaban" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- nominalpertanggungjawaban -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Input Bijak</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="tglib" class="form-control col-md-7 col-xs-12" value=""></input>
		</div>
  </div><!-- tglib -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status Anggaran</label>
		<div class="col-md-6 col-sm-6 col-xs-12">
      <select name="statusanggaran" class="form-control col-md-7 col-xs-12">
        <option value="">-</option>
  			<option value="Pindah Buku">Pindah Buku</option>
  			<option value="Kliring / RTGS">Kliring / RTGS</option>
  			<option value="Dicairkan">Dicairkan</option>
  			<option value="Dipertanggungjawabkan">Dipertanggungjawabkan</option>
      </select>
		</div>
  </div><!-- statusanggaran -->
  <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Status Bijak</label>
		<div class="col-md-6 col-sm-6 col-xs-12" >
      <select name="statusbijak" class="form-control col-md-7 col-xs-12">
        <option value="">-</option>
  			<option value="Belum Diinput">Belum Diinput</option>
  			<option value="Sudah Diinput">Sudah Diinput</option>
      </select>
		</div>
  </div><!-- statusbijak -->
  <div class="item form-group">
	  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Unit Kerja</label>
    <div class="col-md-6 col-sm-6 col-xs-12" >
      <select name="unitkerja" class="form-control col-md-7 col-xs-12">
        <option value="">-</option>
<?php
	  $queryunitkerja = mysqli_query($koneksi, "SELECT * FROM xx_unitkerja WHERE approvaladm = 'Y' AND KetDelete != 'Y'");
	  while($unit = mysqli_fetch_array($queryunitkerja)){
?>
  			<option value="<?php echo $unit['idUnitKerja']; ?>"><?php echo $unit['Keterangan']; ?></option>
<?php
    }
?>
      </select>
    </div>
  </div><!-- unitkerja -->
  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button id="send" type="submit" name="anggaran" class="btn btn-success">Simpan</button>
    </div>
  </div>
</form>
<?php
  }
?>
<script>
function reverseNumber(input){
  return [].map.call(input, function(x){
    return x;
  }).reverse().join(''); 
}
function plainNumber(number){
  return number.split('.').join('');
}
function splitInDots(input){
  var value = input.value,
  plain = plainNumber(value),
  reversed = reverseNumber(plain),
  reversedWithDots = reversed.match(/.{1,3}/g).join('.'),
  normal = reverseNumber(reversedWithDots);
  console.log(plain,reversed, reversedWithDots, normal);
  input.value = normal;
}
</script>
<?php
}
else if($_SESSION['IdRole'] == 1){
  if($menu == 'unitkerja'){
?>
<div class="x_title">
	<h2>Tambah Unit Kerja</h2>
</div>
<form action="simpan.php?menu=unitkerja" method="POST" class="form-horizontal form-label-left" id="myForm">
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Singkatan<span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="singkatanuk" class="form-control col-md-7 col-xs-12" value="" required></input>
		</div>
  </div><!-- singkatanuk -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama / Keterangan Unit Kerja<span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="namauk" class="form-control col-md-7 col-xs-12" value="" required></input>
		</div>
  </div><!-- namauk -->
  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button id="send" type="submit" name="unitkerja" class="btn btn-success">Simpan</button>
    </div>
  </div>
</form>
<?php
  }
  if($menu == 'harilibur'){
?>
<div class="x_title">
	<h2>Tambah Hari Libur</h2>
</div>
<form action="simpan.php?menu=harilibur" method="POST" class="form-horizontal form-label-left" id="myForm">
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Tanggal Libur<span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="tanggal" class="form-control col-md-7 col-xs-12" value="" required></input>
		</div>
  </div><!-- tanggal -->
  <script>
$('input[name="tanggal"]').daterangepicker({
  "singleDatePicker": true,
  "showDropdowns": true,
  "autoApply": true,
  "locale":{
    "format": "MM/DD/YYYY",
    "separator": " - ",
    "daysOfWeek":["M", "S", "S", "R", "K", "J", "S"],
    "monthNames":["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
    "firstDay": 1
  }
},
  function(start, end, label){
    console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
  }
);
  </script>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama / Keterangan Hari Libur<span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="namahl" class="form-control col-md-7 col-xs-12" value="" required></input>
		</div>
  </div><!-- namahl -->
  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button id="send" type="submit" name="harilibur" class="btn btn-success">Simpan</button>
    </div>
  </div>
</form>
<?php
  }
  if($menu == 'userbaru'){
?>
<div class="x_title">
	<h2>Tambah Unit Kerja</h2>
</div>
<form action="simpan.php?menu=unitkerja" method="POST" class="form-horizontal form-label-left" id="myForm">
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Singkatan<span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="singkatanuk" class="form-control col-md-7 col-xs-12" value="" required></input>
		</div>
  </div><!-- singkatanuk -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama / Keterangan Unit Kerja<span class="required">*</span></label>
		<div class="col-md-6 col-sm-6 col-xs-12">
			<input type="text"  name="namauk" class="form-control col-md-7 col-xs-12" value="" required></input>
		</div>
  </div><!-- namauk -->
  <div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button id="send" type="submit" name="unitkerja" class="btn btn-success">Simpan</button>
    </div>
  </div>
</form>
<?php
  }
  
}
else{
	header('location:menu.php');
}
?>