<?php
session_start();
require "koneksi.php";
$menu = $_GET['menu'];
$id = $_GET['id'];
$hapus = $_GET['hapus'];
if($id == NULL){
  header("location:menu.php");
}
else if($_SESSION['IdRole'] == 3){
  if($menu == "sanksi"){
    if($hapus == "ok"){
      header("location:hapus.php?menu=sanksi&id=".$id);
    }
    else{
      $editorial = mysqli_fetch_array(mysqli_query($koneksi, "SELECT approver FROM admsanksi WHERE idadmsanksi = '$id'"));
      if($editorial['approver'] != NULL){
        $editorika = $editorial['approver'].", ".$_SESSION['Name'];
      }
      else{
        $editorika = $_SESSION['Name'];
      }
      mysqli_query($koneksi, "UPDATE admsanksi SET approval = 'Y', approver = '$editorika' WHERE idadmsanksi = '$id'");
      header("location:menu.php?menu=sanksi");
    }
  }
  else if($menu == "anggaran"){
    if($hapus == "ok"){
      header("location:hapus.php?menu=anggaran&id=".$id);
    }
    else{
      $editorial = mysqli_fetch_array(mysqli_query($koneksi, "SELECT approver FROM admanggaran WHERE idadmanggaran = '$id'"));
      if($editorial['approver'] != NULL){
        $editorika = $editorial['approver'].", ".$_SESSION['Name'];
      }
      else{
        $editorika = $_SESSION['Name'];
      }
      mysqli_query($koneksi, "UPDATE admanggaran SET approval = 'Y', approver = '$editorika' WHERE idadmanggaran = '$id'");
      header("location:menu.php?menu=anggaran");
    }
  }
}/* khusus penyetuju */
else if($_SESSION['IdRole'] == 11){
  if($menu == "sanksi"){
    if($hapus == "ok"){
      header("location:hapus.php?menu=sanksi&id=".$id);
    }
    else{
      $editorial = mysqli_fetch_array(mysqli_query($koneksi, "SELECT approver FROM admsanksi WHERE idadmsanksi = '$id'"));
      if($editorial['approver'] != NULL){
        $editorika = $editorial['approver'].", ".$_SESSION['Name'];
      }
      else{
        $editorika = $_SESSION['Name'];
      }
      mysqli_query($koneksi, "UPDATE admsanksi SET approvaladm = 'Y', approver = '$editorika' WHERE idadmsanksi = '$id'");
      header("location:menu.php?menu=sanksi");
    }
  }
  else if($menu == "anggaran"){
    if($hapus == "ok"){
      header("location:hapus.php?menu=anggaran&id=".$id);
    }
    else{
      $editorial = mysqli_fetch_array(mysqli_query($koneksi, "SELECT approver FROM admanggaran WHERE idadmanggaran = '$id'"));
      if($editorial['approver'] != NULL){
        $editorika = $editorial['approver'].", ".$_SESSION['Name'];
      }
      else{
        $editorika = $_SESSION['Name'];
      }
      mysqli_query($koneksi, "UPDATE admanggaran SET approvaladm = 'Y', approver = '$editorika' WHERE idadmanggaran = '$id'");
      header("location:menu.php?menu=anggaran");
    }
  }
  else if($menu == "unitkerja"){
    if($hapus == "ok"){
      header("location:hapus.php?menu=unitkerja&id=".$id);
    }
    else{
      mysqli_query($koneksi, "UPDATE xx_unitkerja SET approvaladm = 'Y' WHERE idUnitKerja = '$id'");
      header("location:menu.php?menu=unitkerja");
    }
  }
  else if($menu == "harilibur"){
    if($hapus == "ok"){
      header("location:hapus.php?menu=harilibur&id=".$id);
    }
    else{
      mysqli_query($koneksi, "UPDATE libur SET approvaladm = 'Y' WHERE idLibur = '$id'");
      header("location:menu.php?menu=harilibur");
    }
  }
}/* khusus adminpenyetuju */
?>