<?php
ob_start();
error_reporting(0);
session_start();
require 'koneksi.php';
if(empty($_SESSION['Id'])){
	if(isset($_POST['submit'])){
    $username = $_POST['usernem'];
    $password = md5($_POST['password']);
    $name = $_POST['name'];
    mysqli_query($koneksi, "INSERT INTO Users (Username, Password, Name, approvaladm) VALUES ('$username', '$password', '$name', 'N')");/* Approve utk adm penyetuju */
    header('location:index.php');
  }/* submit user baru */
  else{
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Administrasi KPwBI Solo (Licensed to BI)</title>
    <link rel="icon" href="bi.ico" type="image/x-icon">
		<link rel="stylesheet" type="text/css" href="Vendor/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="Vendor/css/custom.css">
		<link rel="stylesheet" type="text/css" href="Vendor/css/font-awesome.min.css">
    <script type="text/javascript" src="Vendor/js/jquery.dataTables.js"></script>
    <script src="Vendor/js/jquery.min.js"></script>
    <script src="Vendor/js/custom.js"></script>
    <script src="Vendor/js/bootstrap.min.js"></script>
    <script src="Vendor/js/fastclick.js"></script>
    <script src="Vendor/js/nprogress.js"></script>
    <script src="Vendor/js/validator.min.js"></script>
    <script type="text/javascript" src="Vendor/js/jquery-1.12.0.min.js"></script>
  </head>
  <body class="gambar">
    <div id="wrapper">
      <div id="login" class="form">
        <section class="login_content">
          <h2><img src="Vendor/image/bi.png" width="10%"> BANK INDONESIA</h2>
          <div class="clearfix"></div>
          <form action="login.php" method="POST">
            <h1>Form Login</h1>
            <div><input type="text" class="form-control" placeholder="Username" required="" name="Username" /></div>
            <div><input type="password" class="form-control" placeholder="Password" required="" name="Password" /></div>
            <div><button type="submit" name="submit" value="Login" class="btn btn-info">Log In</button></div>
            <div class="separator">
              <div><button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#myModal">Buat Akun Baru</button></div>
              <br />
              <div><p>Aplikasi Web Administrasi KPwBI Solo</p></div>
            </div>
          </form><!-- login.php -->
        </section>
	    </div>
		  <div class="modal fade" id="myModal" role="dialog">
  		  <div class="modal-dialog">
		      <div class="modal-content">
    		    <div class="modal-header">
        		  <button type="button" class="close" data-dismiss="modal">&times;</button>
      		    <h4 class="modal-title">Tambah Operator</h4>
     		    </div>
     		    <div class="modal-body">
    	        <form action="" method="POST" class="form-horizontal">
								<div class="item form-group">
			            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Anda<span class="required">*</span></label>
           			  <div class="col-md-9 col-sm-9 col-xs-12">
										<input id="name" class="form-control col-md-7 col-xs-12" name="name" placeholder="Masukkan nama asli Anda." type="text" required>
                  </div>
                  </div><!-- nama anda -->
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username<span class="required">*</span></label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input type="text" id="username" name="usernem" class="form-control col-md-7 col-xs-12" placeholder="Masukkan username yang anda kehendaki." required>
                  </div>
                </div><!-- username -->
                <div class="item form-group">
                  <label for="password" class="control-label col-md-3">Password</label>
                  <div class="col-md-9 col-sm-9 col-xs-12">
                    <input id="password" type="password" name="password" class="form-control col-md-7 col-xs-12" required>
                  </div>
                </div><!-- password -->
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                    <button id="submit" type="submit" name="submit" class="btn btn-success">Tambahkan</button>
                  </div>
                </div><!-- tambahkan -->
              </form>
            </div><!-- tmbh oprtr -->
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div><!-- tombol tutup -->
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
<?php
  }/* kl blm login */
}/* sesi kosong */
else{
  header("location:menu.php");
}/* kl dah login */
?>