<?php
ob_start();
date_default_timezone_set("Asia/Jakarta");
session_start();
error_reporting(0);
require "koneksi.php";
if($_SESSION['IdRole'] == 1 or $_SESSION['IdRole'] == 2 or $_SESSION['IdRole'] == 3 or $_SESSION['IdRole'] == 11){
  $queryroleakses = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM xx_role WHERE idRole =".$_SESSION['IdRole']));
  $unit = $_GET['unit'];
  if($unit != NULL){
    $namaunitkerja = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM xx_unitkerja WHERE idUnitKerja = '$unit'"));
    $unitkerjada = "AND unitkerja = '".$unit."'";
  }
  {
  if($_SESSION['IdRole'] == 1){
    $warna = "rgb(255, 255, 255)";
  }
  else if($_SESSION['IdRole'] == 2){
    $warna = "rgb(219, 234, 249)";
    $hidemnjmapproval = 'style="display: none;"';
  }
  else if($_SESSION['IdRole'] == 3){
    $warna = "rgb(185, 255, 185)";
    $hidemnjmapproval = 'style="display: none;"';
    $banyaksanksi = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM admsanksi WHERE approval = 'N' AND approvaladm = 'Y'"));
    $banyakanggaran = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM admanggaran WHERE approval = 'N' AND approvaladm = 'Y'"));
    $banyakadm = $banyaksanksi + $banyakanggaran;
    $approvisasi = "Approval untuk ";
  }
  else if($_SESSION['IdRole'] == 11){
    $warna = "rgb(238, 238, 238)";
    $banyaksanksi = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM admsanksi WHERE approval = 'Y' AND approvaladm = 'N'"));
    $banyakanggaran = mysqli_num_rows(mysqli_query($koneksi, "SELECT * FROM admanggaran WHERE approval = 'Y' AND approvaladm = 'N'"));
    $banyakadm = $banyaksanksi + $banyakanggaran;
    $approvisasi = "Approval untuk ";
  }
  }/* konfigurasi tiap hak akses */
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Administrasi KPwBI Solo</title>
    <link href="bi.ico" rel="icon" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="Vendor/css/bootstrap.min.css" rel="stylesheet">
    <link href="Vendor/css/font-awesome.min.css" rel="stylesheet">
    <link href="Vendor/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="Vendor/css/first.css" rel="stylesheet">
    <link href="Vendor/css/jsdelivr_bootstrap.css" rel="stylesheet">
    <link href="Vendor/css/jsdelivr_daterangepicker.css" rel="stylesheet">
    <script type="text/javascript" src="Vendor/js/jsdelivr_jquery.min.js"></script>
    <script type="text/javascript" src="Vendor/js/jsdelivr_moment.min.js"></script>
    <script type="text/javascript" src="Vendor/js/jsdelivr_daterangepicker.js"></script>
    <script src="Vendor/js/jquery.dataTables.min.js"></script>
    <script src="Vendor/js/bootstrap.min.js"></script>
    <script type="text/javascript">
$(document).ready(function(){
  $('#datatabel').DataTable();
});
    </script>
    <script src="Vendor/js/custom.js"></script>
  </head>
  <body style="background: rgb(247, 247, 247) url('Vendor/image/Win7 LtBlue 1920x1200.jpg') fixed; background-size: cover;">
    <div class="navbar navbar-fixed navbar-fixed-top" role="navigation" style="background-color: <?php echo $warna; ?>; box-shadow: 0px 6px 12px rgba(0, 0, 0, 0.176);">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button><!-- tombol tiga garis -->
          <a class="navbar-brand" href="menu.php">KPwBI Solo</a><!-- home -->
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="?mau=cari"><i class="fa fa-search"></i></a></li>
            <li <?php echo $hidemnjmapproval; ?>>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><strike>Approve</strike> <span class="badge badge-success"></span><b class="caret"></b></a>
              <ul class="dropdown-menu multi-level">
                
              </ul>
            </li><!-- menu approval -->
            <li>
              <a href="#" class="user-profile dropdown-toggle" data-toggle="dropdown">
                <i><?php echo $_SESSION['Name']; ?></i> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu multi-level">
                  <li><a href="?mau=ubahpass">Ubah Password</a></li>
                </ul>
            </li><!-- nama pengguna -->
            <li><a href="logout.php"><i class="fa fa-user"></i> Log Out</a></li><!-- logout -->
          </ul><!-- menu nav kanan -->
          <ul class="nav navbar-nav">
            <li><a href="menu.php">Home</a></li>
            <li>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><strike>Data Statistik</strike> <b class="caret"></b></a>
              <ul class="dropdown-menu">
<?php
  $querydatastatistik = mysqli_query($koneksi, "SELECT idJenisDataStatistik, Keterangan FROM jenisdatastatistik WHERE Approval = 'Y' AND KetDelete != 'Y'");
  while($datas = mysqli_fetch_array($querydatastatistik)){
?>
                <li><a href="?menu=<?php echo strtolower(preg_replace('/\s+/', '', $datas['Keterangan'])); ?>"><?php echo $datas['Keterangan']; ?></a></li>
<?php
  }
?>
              </ul>
            </li><!-- menu data stsk. -->
            <li>
				  		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Administrasi <span class="badge" style="background-color: #337AB7;"><?php echo $banyakadm; ?></span> <b class="caret"></b></a>
              <ul class="dropdown-menu multi-level">
                <li><a href="?menu=sanksi">Sanksi <span class="badge"><?php echo $banyaksanksi; ?></span></a></li>
                <li class="dropdown-submenu"><a href="?menu=anggaran" class="dropdown-toggle">Anggaran <span class="badge"><?php echo $banyakanggaran; ?></span></a>
                  <ul class="dropdown-menu">
<?php
	$queryunitkerja = mysqli_query($koneksi, "SELECT * FROM xx_unitkerja WHERE approvaladm = 'Y' AND KetDelete != 'Y'");
	while($unit = mysqli_fetch_array($queryunitkerja)){
?>
                  	<li><a href="?menu=anggaran&unit=<?php echo $unit['idUnitKerja']; ?>"><?php echo $unit['Keterangan']; ?> <span class="badge"><?php echo $banyakanggaranperunit; ?></span></a></li>
<?php
	}
?>
                 	</ul>
                </li>
              </ul>
            </li><!-- menu adm. sanksi -->
						<li <?php echo $hidemnjmapproval; ?>>
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Manajemen <b class="caret"></b></a>
              <ul class="dropdown-menu">
               	<li><a href="?menu=unitkerja">Unit Kerja</a></li>
                <li><a href="?menu=harilibur">Hari Libur <?php echo date('Y'); ?></a></li>
               	<li class="divider"></li>
               	<li><a href="?menu=pengguna">Pengguna</a></li>
              </ul>
            </li><!-- hny muncul khusus admin -->
          </ul>
        </div>
      </div>
    </div><!-- navbar -->
    <div class="container" style="background-color: rgba(255, 255, 255, 0.7); margin-top: 90px; width: 95%;">
<?php
 	$mau = $_GET['mau'];
 	$menu = $_GET['menu'];
  $id = $_GET['id'];
  if($mau == "tambah"){
    require "tambah.php";
  }/* mau tambah */
  else if($mau == "ubah"){
    require "ubah.php";
  }/* mau ubah */
  else if($mau == "ubahpass"){
    require "pass.php";
  }/* mau ubah password */
  else if($mau == "lihat"){
    require "detail.php";
  }/* mau lihat */
  else if($mau == NULL){
    if($menu == "sanksi"){/* cat: approval=penyetuju, approvaladm=adminpenyetuju */
      if($_SESSION['IdRole'] == 1){
        $_SESSION['Query'] = "SELECT * FROM admsanksi WHERE approval = 'Y' AND ketdelete = 'N' ORDER BY idadmsanksi DESC";
      }
      else if($_SESSION['IdRole'] == 2){
        $_SESSION['Query'] = "SELECT * FROM admsanksi WHERE approvaladm = 'Y' AND ketdelete = 'N' ORDER BY idadmsanksi DESC";
      }
      else if($_SESSION['IdRole'] == 3){
        $_SESSION['Query'] = "SELECT * FROM admsanksi WHERE approval = 'N' AND approvaladm = 'Y'";
      }/* penyetuju */
      else if($_SESSION['IdRole'] == 11){
        $_SESSION['Query'] = "SELECT * FROM admsanksi WHERE approval = 'Y' AND approvaladm = 'N'";
      }
      $querysanksi = mysqli_query($koneksi, $_SESSION['Query']);
?>
<div class="x_title">
  <center>
  	<h2><?php echo $approvisasi; ?>Dokumen Administrasi Sanksi</h2>
<?php
    	if($_SESSION['IdRole'] == 2){
?>
    <a href="?mau=tambah&menu=sanksi" class="btn btn-success btn-ls"><i class="fa fa-plus"></i> Tambahkan</a>
<?php
    	}/* tbl tambah khusus oprator */
?> 
  </center>
</div><!-- judul -->
<table id="datatabel" class="table table-striped table-bordered display hover" cellspacing="0" width="100%">
	<thead style="background-color: <?php echo $warna; ?>; color: #23527C; text-align: center;">
		<tr>
  		<td rowspan="2">No.</td>
			<td rowspan="2">Jenis</td>
			<td colspan="2">Pelapor</td>
			<td rowspan="2">Bulan Data</td>
			<td colspan="2">Kategori</td>
			<td rowspan="2">Sanksi</td>
			<td rowspan="2">Memo Rekom</td>
			<td rowspan="2">No. Surat Sanksi</td>
			<td rowspan="2">Tgl. Surat</td>
			<td rowspan="2">Status Pantau</td>
			<td rowspan="2">Realisasi</td>
			<td rowspan="2">Tgl. Bayar</td>
			<td rowspan="2">Rekening Antara</td>
			<td rowspan="2">Rekening Penerimaan</td>
			<td rowspan="2">Keterangan</td>
			<td rowspan="2">File</td>
			<td rowspan="2">Aksi</td>
		</tr>
		<tr>
			<td>Sandi</td>
			<td>Nama</td>
      <td>Sebab</td>
      <td>Sanksi</td>
		</tr>
	</thead>
<?php
	    $no = 1;
	    while($isi = mysqli_fetch_array($querysanksi)){
        if($isi['sanksi'] == NULL){
          $sanksi = NULL;
        }
        else{
          $sanksi = "Rp. ".$isi['sanksi'].",00";
        }
        if($isi['realisasi'] == NULL){
          $realisasi = NULL;
        }
        else{
          $realisasi = "Rp. ".$isi['realisasi'].",00";
        }
?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $isi['jenispelaporan']; ?></td>
		<td><?php echo $isi['sandipelapor']; ?></td>
		<td><?php echo $isi['namapelapor']; ?></td>
		<td><?php echo $isi['bulandata']; ?></td>
		<td><?php echo $isi['kategorisebab']; ?></td>
		<td><?php echo $isi['kategorisanksi']; ?></td>
		<td><?php echo $sanksi; ?></td>
		<td><?php echo $isi['memorekam']; ?></td>
		<td><?php echo $isi['suratsanksi']; ?></td>
		<td><?php echo $isi['tglsurat']; ?></td>
		<td><?php echo $isi['statuspantau']; ?></td>
		<td><?php echo $realisasi; ?></td>
		<td><?php echo $isi['tglbayar']; ?></td>
		<td><?php echo $isi['rekeningantara']; ?></td>
		<td><?php echo $isi['rekeningpenerimaan']; ?></td>
		<td><?php echo $isi['ket']; ?></td>
		<td><?php echo $isi['file']; ?></td>
		<td>
      <a class="btn btn-round btn-success btn-xs" href="?mau=lihat&menu=sanksi&id=<?php echo $isi['idadmsanksi']; ?>" style="margin: 1px;"><i class="fa fa-eye"></i> Lihat</a>
<?php
        if($_SESSION['IdRole'] == 1 or $_SESSION['IdRole'] == 2){
          if($isi['approval'] == 'N'){
            echo "<br />Belum di-<i>approve</i>.<br />";
          }
          else if($isi['approval'] == 'S'){
            echo "<br />Tidak di-<i>approve</i>, silahkan dicek.<br />";
          }
          if($isi['approvaladm'] == 'N'){
            echo "<br />Belum di-<i>approve</i> admin penyetuju.<br />";
          }
          else if($isi['approvaladm'] == 'S'){
            echo "<br />Tidak di-<i>approve</i> admin penyetuju, silahkan dicek.<br />";
          }
?>
			<a href="?mau=ubah&menu=sanksi&id=<?php echo $isi['idadmsanksi']; ?>" class="btn btn-info btn-xs" style="margin: 1px;"><i class="fa fa-pencil"></i> Ubah</a>
<?php
        }/* oprator ato admin */
        if($_SESSION['IdRole'] == 3 or $_SESSION['IdRole'] == 11){
          echo "<br />Oleh ".$isi['editor'].":";
          if($isi['ketdelete'] == 'Y'){
            echo " (mau dihapus)";
            $hapus = "&hapus=ok";
          }
          else{
            $hapus = NULL;
          }
?>
      <br />
			<a href="setuju.php?menu=sanksi&id=<?php echo $isi['idadmsanksi'].$hapus; ?>" class="btn btn-success btn-xs" style="margin: 1px;"><i class="fa fa-check"></i> Approve</a>
			<a href="tolak.php?menu=sanksi&id=<?php echo $isi['idadmsanksi'].$hapus; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-remove"></i> Reject</a>
<?php
        }/* pnytuju ato admin2 */
        if($_SESSION['IdRole'] != 11){
?>
      <a href="hapus.php?menu=sanksi&id=<?php echo $isi['idadmsanksi']; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-trash-o"></i> Hapus</a>
<?php
        }
?>
    </td><!-- tombol aksi -->
	</tr>
<?php
    	  $no++;
      }
?>
</table><!-- tabel -->
<br />
<div style="text-align: right;">
  <ul class="coba">
    <li style="display: none;">
    	<form method="POST">
        <div class="col-md-6" style="margin-left:-70px;">
          <div class="col-md-6">
            <div class="inner-addon left-addon">
              <i class="fa fa-calendar-check-o"></i>
              <input type="text"   class="form-control" name="datefilter" placeholder="rentang tanggal" style="width: 240px;"/>
            </div>
          </div>
          <div class="col-md-5" style="text-align:left;">
            <input type="submit" name="submittanggal" value="Cari" class="btn btn-primary inli">
          </div>
        </div>                    
      </form>
    </li>
    <li><a href="cetak.php?format=pdf&menu=sanksi" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Simpan PDF</a></li>
    <li><a href="cetak.php?format=xls&menu=sanksi" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Simpan Excel</a></li>
  </ul>
</div><!-- bawah tabel -->
<?php
    }/* menu adm sanksi */
    else if($menu == "anggaran"){
      if($_SESSION['IdRole'] == 1){
        $_SESSION['Query'] = "SELECT * FROM admanggaran WHERE approval = 'Y' AND ketdelete = 'N' ".$unitkerjada." ORDER BY idadmanggaran DESC";
      }
      else if($_SESSION['IdRole'] == 2){
         $_SESSION['Query'] = "SELECT * FROM admanggaran WHERE approvaladm = 'Y' AND ketdelete = 'N' ".$unitkerjada." ORDER BY idadmanggaran DESC";
      }
      else if($_SESSION['IdRole'] == 3){
         $_SESSION['Query'] = "SELECT * FROM admanggaran WHERE approval = 'N' AND approvaladm = 'Y' ".$unitkerjada;
      }
      else if($_SESSION['IdRole'] == 11){
         $_SESSION['Query'] = "SELECT * FROM admanggaran WHERE approval = 'Y' AND approvaladm = 'N' ".$unitkerjada;
      }
      $queryanggaran = mysqli_query($koneksi, $_SESSION['Query']);
?>
<div class="x_title">
  <center>
  	<h2><?php echo $approvisasi; ?>Dokumen Administrasi Anggaran<?php echo " ".$namaunitkerja['Keterangan']; ?></h2>
<?php
    	if($_SESSION['IdRole'] == 2){
?>
    <a href="?mau=tambah&menu=anggaran" class="btn btn-success btn-ls"><i class="fa fa-plus"></i> Tambahkan</a>
<?php
    	}/* tbl tambah khusus oprator */
?> 
  </center>
</div><!-- judul -->
<table id="datatabel" class="table table-striped table-bordered display hover" cellspacing="0" width="100%">
	<thead style="background-color: <?php echo $warna; ?>; color: #23527C; text-align: center;">
		<tr>
  		<td rowspan="2">No.</td>
			<td rowspan="2">Ket. (Kegiatan / Pelaku PDDN)</td>
			<td rowspan="2">Tgl. WRA</td>
			<td rowspan="2">Nominal WRA</td>
			<td rowspan="2">Tgl. Pertanggung-jawaban</td>
			<td rowspan="2">Nominal Pertanggung-jawaban</td>
			<td rowspan="2">Tgl. Input Bijak</td>
			<td colspan="2">Status</td>
			<td rowspan="2">Aksi</td>
		</tr>
    <tr>
      <td>Anggaran</td>
      <td>Bijak</td>
    </tr>
	</thead>
<?php
	    $no = 1;
	    while($isi = mysqli_fetch_array($queryanggaran)){
        if($isi['nominalwra'] == NULL){
          $nominalwra = NULL;
        }
        else{
          $nominalwra = "Rp. ".$isi['nominalwra'].",00";
        }
        if($isi['nominalpertanggungjawaban'] == NULL){
          $nominalpertanggungjawaban = NULL;
        }
        else{
          $nominalpertanggungjawaban = "Rp. ".$isi['nominalpertanggungjawaban'].",00";
        }
?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo nl2br($isi['ketkegiatanpelakupddn']); ?></td>
		<td><?php echo $isi['tglwra']; ?></td>
		<td><?php echo $nominalwra; ?></td>
		<td><?php echo $isi['tglpertanggungjawaban']; ?></td>
		<td><?php echo $nominalpertanggungjawaban; ?></td>
		<td><?php echo $isi['tglinputbijak']; ?></td>
		<td><?php echo $isi['statusanggaran']; ?></td>
		<td><?php echo $isi['statusbijak']; ?></td>
		<td>
      <a class="btn btn-round btn-success btn-xs" href="?mau=lihat&menu=anggaran&id=<?php echo $isi['idadmanggaran']; ?>" style="margin: 1px;"><i class="fa fa-eye"></i> Lihat</a>
<?php
        if($_SESSION['IdRole'] == 1 or $_SESSION['IdRole'] == 2){
          if($isi['approval'] == 'N'){
            echo "<br />Belum di-<i>approve</i>.<br />";
          }
          else if($isi['approval'] == 'S'){
            echo "<br />Tidak di-<i>approve</i>, silahkan dicek.<br />";
          }
          if($isi['approvaladm'] == 'N'){
            echo "<br />Belum di-<i>approve</i> admin penyetuju.<br />";
          }
          else if($isi['approvaladm'] == 'S'){
            echo "<br />Tidak di-<i>approve</i> admin penyetuju, silahkan dicek.<br />";
          }
?>
			<a href="?mau=ubah&menu=anggaran&id=<?php echo $isi['idadmanggaran']; ?>" class="btn btn-info btn-xs" style="margin: 1px;"><i class="fa fa-pencil"></i> Ubah</a>
<?php
        }/* oprator ato admin */
        if($_SESSION['IdRole'] == 3 or $_SESSION['IdRole'] == 11){
          echo "<br />Oleh ".$isi['editor'].":";
          if($isi['ketdelete'] == 'Y'){
            echo " (mau dihapus)";
            $hapus = "&hapus=ok";
          }
          else{
            $hapus = NULL;
          }
?>
      <br />
			<a href="setuju.php?menu=anggaran&id=<?php echo $isi['idadmanggaran'].$hapus; ?>" class="btn btn-success btn-xs" style="margin: 1px;"><i class="fa fa-check"></i> Approve</a>
			<a href="tolak.php?menu=anggaran&id=<?php echo $isi['idadmanggaran'].$hapus; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-remove"></i> Reject</a>
<?php
        }/* pnytuju ato admin2 */
        if($_SESSION['IdRole'] != 11){
?>
      <a href="hapus.php?menu=anggaran&id=<?php echo $isi['idadmanggaran']; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-trash-o"></i> Hapus</a>
<?php
        }
?>
    </td><!-- tombol aksi -->
	</tr>
<?php
    	  $no++;
      }
?>
</table><!-- tabel -->
<br />
<div style="text-align: right;">
  <ul class="coba">
    <li style="display: none;">
    	<form method="POST">
        <div class="col-md-6" style="margin-left:-70px;">
          <div class="col-md-6">
            <div class="inner-addon left-addon">
              <i class="fa fa-calendar-check-o"></i>
              <input type="text"   class="form-control" name="datefilter" placeholder="rentang tanggal" style="width: 240px;"/>
            </div>
          </div>
          <div class="col-md-5" style="text-align:left;">
            <input type="submit" name="submittanggal" value="Cari" class="btn btn-primary inli">
          </div>
        </div>                    
      </form>
    </li>
    <li><a href="cetak.php?format=pdf&menu=anggaran" class="btn btn-danger"><i class="fa fa-file-pdf-o"></i> Simpan PDF</a></li>
    <li><a href="cetak.php?format=xls&menu=anggaran" class="btn btn-info"><i class="fa fa-file-excel-o"></i> Simpan Excel</a></li>
  </ul>
</div><!-- bawah tabel -->
<?php
    }/* menu adm anggaran */
    else if($menu == "unitkerja"){/* cat: approval=penyetuju, approvaladm=adminpenyetuju */
      if($_SESSION['IdRole'] == 1){
        $queryunitkerja = mysqli_query($koneksi, "SELECT * FROM xx_unitkerja WHERE ketdelete = 'N'");
      }
      else if($_SESSION['IdRole'] == 11){
        $queryunitkerja = mysqli_query($koneksi, "SELECT * FROM xx_unitkerja WHERE approvaladm = 'N'");
      }
?>
<div class="x_title">
  <center>
  	<h2><?php echo $approvisasi; ?>Manajemen Unit Kerja</h2>
<?php
    	if($_SESSION['IdRole'] == 1){
?>
    <a href="?mau=tambah&menu=unitkerja" class="btn btn-success btn-ls"><i class="fa fa-plus"></i> Tambahkan</a>
<?php
    	}/* tbl tambah khusus admin */
?> 
  </center>
</div><!-- judul -->
<table id="datatabel" class="table table-striped table-bordered display hover" cellspacing="0" width="100%">
	<thead style="background-color: <?php echo $warna; ?>; color: #23527C; text-align: center;">
  	<tr>
      <td>No.</td>
			<td>Singkatan</td>
			<td>Ket. Unit Kerja</td>
			<td>Aksi</td>
    </tr>
	</thead>
<?php
	    $no = 1;
	    while($isi = mysqli_fetch_array($queryunitkerja)){
?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $isi['UnitKerja']; ?></td>
		<td><?php echo $isi['Keterangan']; ?></td>
		<td>
<?php
        if($_SESSION['IdRole'] == 1){
          if($isi['approvaladm'] == 'N'){
            echo "Belum di-<i>approve</i> admin penyetuju.<br />";
          }
          else if($isi['approvaladm'] == 'S'){
            echo "Tidak di-<i>approve</i> admin penyetuju, silahkan dicek.<br />";
          }
?>
			<a href="?mau=ubah&menu=unitkerja&id=<?php echo $isi['idUnitKerja']; ?>" class="btn btn-info btn-xs" style="margin: 1px;"><i class="fa fa-pencil"></i> Ubah</a>
<?php
        }/* admin */
        if($_SESSION['IdRole'] == 11){
          echo "Oleh ".$isi['IdUserCreate'].":";
          if($isi['KetDelete'] == 'Y'){
            echo " (mau dihapus)";
            $hapus = "&hapus=ok";
          }
          else{
            $hapus = NULL;
          }
?>
      <br />
			<a href="setuju.php?menu=unitkerja&id=<?php echo $isi['idUnitKerja'].$hapus; ?>" class="btn btn-success btn-xs" style="margin: 1px;"><i class="fa fa-check"></i> Approve</a>
			<a href="tolak.php?menu=unitkerja&id=<?php echo $isi['idUnitKerja'].$hapus; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-remove"></i> Reject</a>
<?php
        }/* admin penyetuju */
        if($_SESSION['IdRole'] == 1){
?>
      <a href="hapus.php?menu=unitkerja&id=<?php echo $isi['idUnitKerja']; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-trash-o"></i> Hapus</a>
<?php
        }
?>
    </td><!-- tombol aksi -->
	</tr>
<?php
    	  $no++;
      }
?>
</table><!-- tabel -->
<br />
<?php
    }/* menu mnjmn uk */
    else if($menu == "harilibur"){/* cat: approval=penyetuju, approvaladm=adminpenyetuju */
      $tahun = date("Y");
      if($_SESSION['IdRole'] == 1){
        $queryharilibur = mysqli_query($koneksi, "SELECT * FROM libur WHERE Tahun = '$tahun' AND KetDelete = 'N'");
      }
      else if($_SESSION['IdRole'] == 11){
        $queryharilibur = mysqli_query($koneksi, "SELECT * FROM libur WHERE Tahun = '$tahun' AND approvaladm = 'N'");
      }
?>
<div class="x_title">
  <center>
  	<h2><?php echo $approvisasi; ?>Manajemen Hari Libur <?php echo $tahun; ?></h2>
<?php
    	if($_SESSION['IdRole'] == 1){
?>
    <a href="?mau=tambah&menu=harilibur" class="btn btn-success btn-ls"><i class="fa fa-plus"></i> Tambahkan</a>
<?php
    	}/* tbl tambah khusus admin */
?> 
  </center>
</div><!-- judul -->
<table id="datatabel" class="table table-striped table-bordered display hover" cellspacing="0" width="100%">
	<thead style="background-color: <?php echo $warna; ?>; color: #23527C; text-align: center;">
  	<tr>
      <td>No.</td>
			<td>Tanggal</td>
			<td>Ket. Hari Libur</td>
			<td>Aksi</td>
    </tr>
	</thead>
<?php
	    $no = 1;
	    while($isi = mysqli_fetch_array($queryharilibur)){
?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $isi['Tanggal']; ?></td>
		<td><?php echo $isi['Keterangan']; ?></td>
		<td>
<?php
        if($_SESSION['IdRole'] == 1){
          if($isi['approvaladm'] == 'N'){
            echo "Belum di-<i>approve</i> admin penyetuju.<br />";
          }
          else if($isi['approvaladm'] == 'S'){
            echo "Tidak di-<i>approve</i> admin penyetuju, silahkan dicek.<br />";
          }
?>
			<a href="?mau=ubah&menu=harilibur&id=<?php echo $isi['idLibur']; ?>" class="btn btn-info btn-xs" style="margin: 1px;"><i class="fa fa-pencil"></i> Ubah</a>
<?php
        }/* admin */
        if($_SESSION['IdRole'] == 11){
          echo "Oleh ".$isi['idUserCreate'].":";
          if($isi['KetDelete'] == 'Y'){
            echo " (mau dihapus)";
            $hapus = "&hapus=ok";
          }
          else{
            $hapus = NULL;
          }
?>
      <br />
			<a href="setuju.php?menu=harilibur&id=<?php echo $isi['idLibur'].$hapus; ?>" class="btn btn-success btn-xs" style="margin: 1px;"><i class="fa fa-check"></i> Approve</a>
			<a href="tolak.php?menu=harilibur&id=<?php echo $isi['idLibur'].$hapus; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-remove"></i> Reject</a>
<?php
        }/* admin penyetuju */
        if($_SESSION['IdRole'] == 1){
?>
      <a href="hapus.php?menu=harilibur&id=<?php echo $isi['idLibur']; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-trash-o"></i> Hapus</a>
<?php
        }
?>
    </td><!-- tombol aksi -->
	</tr>
<?php
    	  $no++;
      }
?>
</table><!-- tabel -->
<br />
<?php
    }/* menu mnjmn hl */
    else if($menu == "pengguna"){/* cat: approval=penyetuju, approvaladm=adminpenyetuju */
      if($_SESSION['IdRole'] == 1){
        $querypengguna = mysqli_query($koneksi, "SELECT * FROM users ORDER BY idUser DESC");
      }
      else if($_SESSION['IdRole'] == 11){
        $querypengguna = mysqli_query($koneksi, "SELECT * FROM users WHERE approvaladm = 'N'");
      }
?>
<div class="x_title">
  <center>
  	<h2>Manajemen Pengguna</h2>
  </center>
</div><!-- judul -->
<table id="datatabel" class="table table-striped table-bordered display hover" cellspacing="0" width="100%">
	<thead style="background-color: <?php echo $warna; ?>; color: #23527C; text-align: center;">
  	<tr>
      <td>No.</td>
			<td>Nama Orang</td>
			<td>Username</td>
			<td>Status, Id Akses</td>
      <td>Aksi</td>
    </tr>
	</thead>
<?php
	    $no = 1;
	    while($isi = mysqli_fetch_array($querypengguna)){
?>
	<tr>
		<td><?php echo $no; ?></td>
		<td><?php echo $isi['Name']; ?></td>
		<td><?php echo $isi['Username']; ?></td>
<?php
        if($isi['approvaladm'] != "Y"){
?>
    <td>Non-aktif</td>
    <td>
      <form action="user.php?mau=aktifkan&id=<?php echo $isi['idUser']; ?>" method="POST">
    	  <button class="btn btn-success btn-xs" type="submit"><i class="fa fa-check"></i> Aktifkan</button> sebagai 
        <select name="rolepengguna">
<?php
	        $queryrolepengguna = mysqli_query($koneksi, "SELECT * FROM xx_role");
	        while($hak = mysqli_fetch_array($queryrolepengguna)){
?>
  			  <option value="<?php echo $hak['idRole']; ?>"><?php echo strtolower($hak['roleAkses']); ?></option>
<?php
          }
?>
        </select>
      </form>
      <a href="hapus.php?menu=pengguna&id=<?php echo $isi['idUser']; ?>" class="btn btn-danger btn-xs" style="margin: 1px;"><i class="fa fa-trash-o"></i> Hapus</a>
    </td>
<?php
        }
        else{
          if($isi['idUser'] == $_SESSION['Id']){
            $danlogout = " dan log out";
            $logoutkan = "&log=out";
          }
          else{
            $danlogout = NULL;
            $logoutkan = NULL;
          }
?>
    <td>Aktif, <?php echo $isi['RoleAkses']; ?></td> 
    <td><form action="user.php?mau=nonaktifkan&id=<?php echo $isi['idUser'].$logoutkan; ?>" method="POST"><button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-remove"></i> Non-aktifkan<?php echo $danlogout; ?></button></form></td>    
	</tr>
<?php
    	  }
        $no++;
      }
?>
</table><!-- tabel -->
<br />
<?php
    }/* menu mnjmn pengguna */
    else{
?>
<center>Halaman untuk <?php echo strtolower($queryroleakses['roleAkses']); ?>.</center>
<p><?php echo nl2br($queryroleakses['catatan']); ?></p>
<?php
    }
  }
  else{
?>
      <h2>404</h2>
      Maaf, halaman tidak ditemukan / terjadi kesalahan.
      <br />
      <a href="javascript:history.back()"><button class="btn btn-info"><span class="fa fa-backward"></span> Kembali</button></a>
      <br />
      <br />
<?php
  }
}/* pgguna terijinkan */
else{
  header("location:logout.php");
}
?>
    </div><!-- menu-footer -->
  </body>
</html>