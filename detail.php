<?php
if($_SESSION['IdRole'] == 1 or $_SESSION['IdRole'] == 2 or $_SESSION['IdRole'] == 3 or $_SESSION['IdRole'] == 11){
  if($menu == "sanksi"){
    $querysanksi = mysqli_query($koneksi, "SELECT * FROM admsanksi WHERE idadmsanksi = '$id'");
    $isi = mysqli_fetch_array($querysanksi);
    if($isi['sanksi'] == NULL){
      $sanksi = NULL;
    }
    else{
      $sanksi = "Rp. ".$isi['sanksi'].",00";
    }
?>
<div class="x_title">
	<h2><?php echo $isi['suratsanksi']; ?></h2>
</div>
<table class="table">
  <tr><td>Jenis Pelaporan</td><td>:</td><td><?php echo $isi['jenispelaporan']; ?></td></tr>
	<tr><td>Sandi Pelapor</td><td>:</td><td><?php echo $isi['sandipelapor']; ?></td></tr>
	<tr><td>Nama Pelapor</td><td>:</td><td><?php echo $isi['namapelapor']; ?></td></tr>
	<tr><td>Bulan Data</td><td>:</td><td><?php echo $isi['bulandata']; ?></td></tr>
	<tr><td>Kategori Sebab</td><td>:</td><td><?php echo $isi['kategorisebab']; ?></td></tr>
	<tr><td>Kategori Sanksi</td><td>:</td><td><?php echo $isi['kategorisanksi']; ?></td></tr>
	<tr><td>Sanksi</td><td>:</td><td><?php echo $sanksi; ?></td></tr>
	<tr><td>Memo Rekom</td><td>:</td><td><?php echo $isi['memorekam']; ?></td></tr>
	<tr><td>Tanggal Surat</td><td>:</td><td><?php echo $isi['tglsurat']; ?></td></tr>
	<tr><td>Status Pantau</td><td>:</td><td><?php echo $isi['statuspantau']; ?></td></tr>
	<tr><td>Realisasi</td><td>:</td><td><?php echo $isi['realisasi']; ?></td></tr>
	<tr><td>Tanggal Bayar</td><td>:</td><td><?php echo $isi['tglbayar']; ?></td></tr>
	<tr><td>Rekening Antara</td><td>:</td><td><?php echo $isi['rekeningantara']; ?></td></tr>
	<tr><td>Rekening Penerimaan</td><td>:</td><td><?php echo $isi['rekeningpenerimaan']; ?></td></tr>
	<tr><td>Keterangan</td><td>:</td><td><?php echo $isi['ket']; ?></td></tr>
	<tr><td>Lokasi <i>File</i> di <i>Server</i></td><td>:</td><td><?php echo $isi['file']; ?></td></tr>
	<tr><td>Pembuat</td><td>:</td><td><?php echo $isi['pembuat']; ?></td></tr>
	<tr><td>Editor</td><td>:</td><td><?php echo $isi['editor']; ?></td></tr>
	<tr><td>Approver</td><td>:</td><td><?php echo $isi['approver']; ?></td></tr>
</table>
<a href="javascript:history.back()"><button class="btn btn-info"><span class="fa fa-backward"></span> Kembali</button></a>
<br />
<br />
<?php
  }
  else if($menu == 'anggaran'){
    $queryanggaran = mysqli_query($koneksi, "SELECT * FROM admanggaran WHERE idadmanggaran = '$id'");
    $isi = mysqli_fetch_array($queryanggaran);
    $namaunitkerja = mysqli_fetch_array(mysqli_query($koneksi, "SELECT * FROM xx_unitkerja WHERE idUnitKerja = '$isi[unitkerja]'"));
    if($isi['nominalwra'] == NULL){
      $nominalwra = NULL;
    }
    else{
      $nominalwra = "Rp. ".$isi['nominalwra'].",00";
    }
    if($isi['nominalpertanggungjawaban'] == NULL){
      $nominalpertanggungjawaban = NULL;
    }
    else{
      $nominalpertanggungjawaban = "Rp. ".$isi['nominalpertanggungjawaban'].",00";
    }
?>
<div class="x_title">
	<h2><?php echo nl2br($isi['ketkegiatanpelakupddn']); ?></h2>
</div>
<table class="table">
  <tr><td>Tanggal WRA</td><td>:</td><td><?php echo $isi['tglwra']; ?></td></tr>
	<tr><td>Nominal WRA</td><td>:</td><td><?php echo $nominalwra; ?></td></tr>
	<tr><td>Tanggal Pertanggungjawaban</td><td>:</td><td><?php echo $isi['tglpertanggungjawaban']; ?></td></tr>
	<tr><td>Nominal Pertanggungjawaban</td><td>:</td><td><?php echo $nominalpertanggungjawaban; ?></td></tr>
	<tr><td>Tanggal Input Bijak</td><td>:</td><td><?php echo $isi['tglinputbijak']; ?></td></tr>
	<tr><td>Status Anggaran</td><td>:</td><td><?php echo $isi['statusanggaran']; ?></td></tr>
	<tr><td>Status Bijak</td><td>:</td><td><?php echo $isi['statusbijak']; ?></td></tr>
	<tr><td>Unit Kerja</td><td>:</td><td><?php echo $namaunitkerja['Keterangan']; ?></td></tr>
	<tr><td>Pembuat</td><td>:</td><td><?php echo $isi['pembuat']; ?></td></tr>
	<tr><td>Editor</td><td>:</td><td><?php echo $isi['editor']; ?></td></tr>
	<tr><td>Approver</td><td>:</td><td><?php echo $isi['approver']; ?></td></tr>
</table>
<a href="javascript:history.back()"><button class="btn btn-info"><span class="fa fa-backward"></span> Kembali</button></a>
<br />
<br />
<?php
  }
}
?>