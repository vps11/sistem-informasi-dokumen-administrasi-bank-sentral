<?php
session_start();
require "koneksi.php";
$query = mysqli_query($koneksi, "Select * from admsanksi");

?>
<div class="table-responsive">
	<table class="table table-striped" border="1">
		<thead>
			<tr>
				<td rowspan="2">Jenis Pelaporan</td>
			    <td colspan="2">Pelapor</td>
				<td rowspan="2">Bulan Data</td>
				<td rowspan="2">Kategori Sebab</td>
				<td rowspan="2">Kategori Sanksi</td>
				<td rowspan="2">Sanksi</td>
				<td rowspan="2">Memo Rekom Sanksi</td>
				<td rowspan="2">Surat Sanksi</td>
				<td rowspan="2">Tanggal Surat</td>
				<td rowspan="2">Status Pantau</td>
				<td rowspan="2">Realisasi Akhir Sanksi</td>
				<td rowspan="2">Tanggal Bayar</td>
				<td rowspan="2">Rekening Antara</td>
				<td rowspan="2">Rekening Penerimaan</td>
				<td rowspan="2">Keterangan</td>
				<td rowspan="2">File</td>
			</tr>
			<tr>
				<td>Sandi Pelaporan</td>
				<td>Nama Pelaporan</td>
			</tr>
		</thead>
		<tbody>
			<?php while ($isi = mysqli_fetch_assoc($query)){ ?>
				<tr>
					<td><?php echo $isi['jenispelaporan'] ?></td>
					<td><?php echo $isi['sandi_pelapor'] ?></td>
					<td><?php echo $isi['nama_pelapor'] ?></td>
					<td><?php echo $isi['bulandata'] ?></td>
					<td><?php echo $isi['kategorisebab'] ?></td>
					<td><?php echo $isi['kategorisanksi'] ?></td>
					<td><?php echo $isi['sanksi'] ?></td>
					<td><?php echo $isi['memorekam'] ?></td>
					<td><?php echo $isi['suratsanksi'] ?></td>
					<td><?php echo $isi['tglsurat'] ?></td>
					<td><?php echo $isi['statuspantau'] ?></td>
					<td><?php echo $isi['realisasi'] ?></td>
					<td><?php echo $isi['tglbayar'] ?></td>
					<td><?php echo $isi['rekeningantara'] ?></td>
					<td><?php echo $isi['rekeningpenerimaan'] ?></td>
					<td><?php echo $isi['ket'] ?></td>
					<td><?php echo $isi['file'] ?></td>
				</tr>
			<?php }?>
		</tbody>
	</table>	
</div>
</body>
</html>